package main;

import gui.*;

public class Main {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Login login = new Login();
        Cliente cliente = new Cliente();
        Proveedor proveedor = new Proveedor();
        Vista vista = new Vista();

        Controlador controlador = new Controlador(modelo, vista, login, cliente, proveedor);
    }
}
