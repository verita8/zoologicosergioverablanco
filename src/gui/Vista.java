package gui;

import baseEnums.CargoEmpleados;
import baseEnums.EnumUsuarios;
import baseEnums.HabitatsAnimales;
import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Vista  extends JFrame{
    private final static String TITULOFRAME = "Zoológico Sergio Vera Blanco";
    private JTabbedPane tabbedPane1;
    private JPanel panel1;

    public  JTextField txtNombreEmpleado;
    public  JTextField txtApellidosEmpleado;
    public  JTextField txtTelefonoEmpleado;
    public  JTextField txtDniEmpleado;
    public  JTextField txtDireccionEmpleado;
    public  JTextField txtSegudiradSocial;
    public  JTextField txtEmailEmpleado;
    public   JTextField txtNombreCliente;
    public  JTextField txtApellidosCliente;
    public   JTextField txtDniCliente;
    public   JTextField txtTelefonoCliente;
    public  JTextField txtEmailCliente;
    public  JTextField txtDireccionCliente;
    public  JTextField txtNombreProveedor;
    public   JTextField txtTelefonoProveedor;
    public  JTextField txtCiudad;
    public JTextField txtMercancia;
    public JTextField txtCodigoPostal;
    public JTextField txtPrecio;
    public JTextField txtNombreAnimal;
    public JTextField txtEspecie;
    public JTextField txtIdentificador;
    public JTextField txtEdad;
    public JTextField txtSexo;
    public DatePicker fechaCliente;
    public DatePicker fechaEmpleado;
    public DatePicker fechaProveedores;
    public JButton btnEmpleadosAnadir;
    public JButton btnEmpleadosModificar;
    public JButton btnEmpleadosEliminar;
    public JTable empleadosTabla;
    public JButton btnClientesAnadir;
    public JButton btnClientesModificar;
    public JButton btnClientesEliminar;
    public JTable clientesTabla;
    public JButton btnProveedoresAnadir;
    public JButton btnProveedoresModificar;
    public JButton btnProveedoresEliminar;
    public JTable proveedoresTabla;
    public JButton btnAnimalesAnadir;
    public JButton btnAnimalesModificar;
    public JButton btnAnimalesEliminar;
    public JButton btnNavegarLogin;
    public JTable animalesTabla;
    public JTable animalesBuscar;
     public JComboBox comboHabitat;
     public JComboBox comboFuncion;

     //Buscar por nombre del animal
     public JButton btnBuscarNombreAnimal;
    public JTextField txtBuscar;

    public JTextField txtUsuarioRegistro;
    public JPasswordField txtContrasenaRegistro;
    public JButton btnRegistrarse;
    public JComboBox comboUsuarioRegistro;
    public JButton btnGenerarFactura;



    /*MENU*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmAnimales;
    DefaultTableModel dtmEmpleados;
    DefaultTableModel dtmClientes;
    DefaultTableModel dtmProveedores;
    DefaultTableModel dtmBuscarAnimales;

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        //this.setIconImage(new ImageIcon(getClass().getResource("/logoZOO.PNG")).getImage());
        optionDialog = new OptionDialog(this);
        this.setEnumComboBox();
        setMenu();
        setAdminDialog();
        setTableModels();
    }

    private void setTableModels() {

        this.dtmAnimales = new DefaultTableModel();
        this.animalesTabla.setModel(dtmAnimales);

        this.dtmEmpleados = new DefaultTableModel();
        this.empleadosTabla.setModel(dtmEmpleados);

        this.dtmClientes = new DefaultTableModel();
        this.clientesTabla.setModel(dtmClientes);

        this.dtmProveedores = new DefaultTableModel();
        this.proveedoresTabla.setModel(dtmProveedores);

        this.dtmBuscarAnimales =new DefaultTableModel();
        this.animalesBuscar.setModel(dtmBuscarAnimales);
    }

    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setEnumComboBox() {
        for(CargoEmpleados constant : CargoEmpleados.values()) { comboFuncion.addItem(constant.getValor()); }
        comboFuncion.setSelectedIndex(-1);

        for(HabitatsAnimales constant : HabitatsAnimales.values()) { comboHabitat.addItem(constant.getValor()); }
        comboHabitat.setSelectedIndex(-1);

        for(EnumUsuarios constant : EnumUsuarios.values()) { comboUsuarioRegistro.addItem(constant.getValor()); }
        comboUsuarioRegistro.setSelectedIndex(-1);
    }

        private void setAdminDialog() {
            btnValidate = new JButton("Validar");
            btnValidate.setActionCommand("abrirOpciones");
            adminPassword = new JPasswordField();
            adminPassword.setPreferredSize(new Dimension(100, 26));
            Object[] options = new Object[] {adminPassword, btnValidate};
            JOptionPane jop = new JOptionPane("Introduce la contraseña"
                    , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

            adminPasswordDialog = new JDialog(this, "Opciones", true);
            adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            adminPasswordDialog.setContentPane(jop);
            adminPasswordDialog.pack();
            adminPasswordDialog.setLocationRelativeTo(this);

        }
}
