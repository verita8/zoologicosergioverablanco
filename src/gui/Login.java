package gui;

import baseEnums.CargoEmpleados;
import baseEnums.EnumUsuarios;
import baseEnums.HabitatsAnimales;
import main.Main;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Login extends JFrame{
    private static final String TITULOFRAME = "Inicio de sesión";
    private JPanel panel1;
    public JTextField txtUsuario;
    public JPasswordField txtContrasena;
    public JButton btnCancelar;
    public JButton btnAceptar;
    public JComboBox comboUsuarios;

    public Login() {
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        //this.setSize(new Dimension(this.getWidth()+100, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        //this.setIconImage(new ImageIcon(getClass().getResource("/logoZOO.PNG")).getImage());
        this.setEnumComboBox();

        btnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private void setEnumComboBox() {
        for(EnumUsuarios constant : EnumUsuarios.values()) { comboUsuarios.addItem(constant.getValor()); }
        comboUsuarios.setSelectedIndex(-1);
    }
}
