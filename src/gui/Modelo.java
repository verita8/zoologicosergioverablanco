package gui;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    /**
     * Realizamos la conexión a la base de datos de nuesto programa
     */
    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/tfgzoo",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Insertamos los datos del login
     * @param nombre
     * @param contrasena
     * @param usuario
     * @return
     * @throws SQLException
     */
    public int insertarDatos(String nombre, String contrasena, String usuario) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "INSERT INTO login(nombre, contrasena, usuario) VALUES (?,?,?)";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, nombre);
        sentencia.setString(2, contrasena);
        sentencia.setString(2, usuario);
        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return numeroRegistros;
    }

    /**
     * Comprobamos los datos del login
     * @param nombre
     * @param contrasena
     * @param usuario
     * @return
     */
    public boolean comprobarDatos(String nombre, String contrasena, String usuario)  {
        PreparedStatement sentencia = null;
       try {
           String consulta = "SELECT * FROM login where nombre = ? and contrasena = ? and usuario = ?";
           sentencia = conexion.prepareStatement(consulta);
           sentencia.setString(1,nombre);
           sentencia.setString(2,contrasena);
           sentencia.setString(3,usuario);

           ResultSet resultado = sentencia.executeQuery();

           if(resultado.next()) {
               return true;
           }

       } catch (SQLException a) {
           a.printStackTrace();
       }
       return false;
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("zoologico_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Desconectamos de la base de datos
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * Insertamos un empleado
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param telefono
     * @param dni
     * @param direccion
     * @param segudirad_social
     * @param funcion
     * @param email
     */
    void insertarEmpleado(String nombre, String apellidos, LocalDate fecha_nacimiento, int telefono, String dni,
                          String direccion, int segudirad_social, String funcion, String email) {
        String sentenciaSql = "INSERT INTO empleados (nombre, apellidos, fecha_nacimiento, telefono, dni,direccion," +
                "seguridad_social, funcion, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setInt(4, telefono);
            sentencia.setString(5, dni);
            sentencia.setString(6, direccion);
            sentencia.setInt(7, segudirad_social);
            sentencia.setString(8, funcion);
            sentencia.setString(9, email);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificamos un empleado
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param telefono
     * @param dni
     * @param direccion
     * @param segudirad_social
     * @param funcion
     * @param email
     * @param idempleado
     */
    void modificarEmpleado(String nombre, String apellidos, LocalDate fecha_nacimiento, int telefono, String dni,
                           String direccion, int segudirad_social, String funcion, String email, int idempleado) {
        String sentenciaSql = "UPDATE empleados SET nombre = ?, apellidos = ?, fecha_nacimiento = ?, telefono =?, dni = ?, direccion = ?," +
                "seguridad_social = ?, funcion = ?, email = ? WHERE idempleado = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setInt(4, telefono);
            sentencia.setString(5, dni);
            sentencia.setString(6, direccion);
            sentencia.setInt(7, segudirad_social);
            sentencia.setString(8, funcion);
            sentencia.setString(9, email);
            sentencia.setInt(10, idempleado);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Borramos un empleado
     * @param idempleado
     */
    void borrarEmpleado(int idempleado) {
        String sentenciaSql = "DELETE FROM empleados WHERE idempleado = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idempleado);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Consultamos un empleado
     * @return
     * @throws SQLException
     */
    ResultSet consultarEmpleado() throws SQLException {
        String sentenciaSql = "SELECT concat(idempleado) as 'ID', concat(nombre) as 'Nombre empleado', concat(apellidos) as 'Apellidos', " +
                "concat(fecha_nacimiento) as 'Fecha de nacimiento', concat(telefono) as 'Teléfono', concat(dni) as 'DNI'" +
                ", concat(direccion) as 'Dirección' , concat(seguridad_social) as 'Nº Seguridad social' , concat(funcion) as 'Función' " +
                ", concat(email) as 'Email' FROM empleados";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Insertamos un cliente
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param telefono
     * @param dni
     * @param direccion
     * @param email
     */
    void insertarCliente(String nombre, String apellidos, LocalDate fecha_nacimiento, int telefono, String dni,
                          String direccion, String email) {
        String sentenciaSql = "INSERT INTO clientes (nombre, apellidos, fecha_nacimiento, telefono, dni,direccion, " +
                "email) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setInt(4, telefono);
            sentencia.setString(5, dni);
            sentencia.setString(6, direccion);
            sentencia.setString(7, email);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificamos un cliente
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param telefono
     * @param dni
     * @param direccion
     * @param email
     * @param idcliente
     */
    void modificarCliente(String nombre, String apellidos, LocalDate fecha_nacimiento, int telefono, String dni,
                          String direccion, String email, int idcliente){

        String sentenciaSql = "UPDATE clientes SET nombre = ?, apellidos = ?, fecha_nacimiento = ?, telefono = ?, dni = ?, direccion = ?, email = ?" +
                "WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
            sentencia.setInt(4, telefono);
            sentencia.setString(5, dni);
            sentencia.setString(6, direccion);
            sentencia.setString(7, email);
            sentencia.setInt(8, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Borramos un cliente
     * @param idcliente
     */
    void borrarCliente(int idcliente) {
        String sentenciaSql = "DELETE FROM clientes WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Consultamos un cliente
     * @return
     * @throws SQLException
     */
    ResultSet consultarCliente() throws SQLException {
        String sentenciaSql = "SELECT concat(idcliente) as 'ID', concat(nombre) as 'Nombre cliente', concat(apellidos) as 'Apellidos', " +
                "concat(dni) as 'DNI', concat(telefono) as 'Teléfono', concat(email) as 'Email'" +
                ", concat(direccion) as 'Dirección' , concat(fecha_nacimiento) as 'Fecha de nacimiento'" +
                " FROM clientes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Insertamos un proveedor
     * @param nombre_empresa
     * @param telefono
     * @param ciudad
     * @param mercancia
     * @param fecha_pedido
     * @param codigo_postal
     * @param precio
     */
    void insertarProveedor(String nombre_empresa,int telefono, String ciudad,String mercancia, LocalDate fecha_pedido,
            String codigo_postal, float precio) {
        String sentenciaSql = "INSERT INTO proveedores (nombre_empresa, telefono, ciudad, mercancia, fecha_pedido, codigo_postal, " +
                "precio) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_empresa);
            sentencia.setInt(2, telefono);
            sentencia.setString(3, ciudad);
            sentencia.setString(4, mercancia);
            sentencia.setDate(5, Date.valueOf(fecha_pedido));
            sentencia.setString(6, codigo_postal);
            sentencia.setFloat(7, precio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificamos un proveedor
     * @param nombre_empresa
     * @param telefono
     * @param ciudad
     * @param mercancia
     * @param fecha_pedido
     * @param codigo_postal
     * @param precio
     * @param idproveedor
     */
    void modificarProveedor(String nombre_empresa,int telefono, String ciudad,String mercancia, LocalDate fecha_pedido,
                            String codigo_postal, float precio, int idproveedor){

        String sentenciaSql = "UPDATE proveedores SET nombre_empresa = ?, telefono = ?, ciudad = ?, mercancia = ?, fecha_pedido = ?, codigo_postal = ?" +
                ",precio = ? WHERE idproveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_empresa);
            sentencia.setInt(2, telefono);
            sentencia.setString(3, ciudad);
            sentencia.setString(4, mercancia);
            sentencia.setDate(5, Date.valueOf(fecha_pedido));
            sentencia.setString(6, codigo_postal);
            sentencia.setFloat(7, precio);
            sentencia.setInt(8, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     * Borramos un proveedor
     * @param idproveedor
     */
    void borrarProveedor(int idproveedor) {
        String sentenciaSql = "DELETE FROM proveedores WHERE idproveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Consultamos un proveedor
     * @return
     * @throws SQLException
     */
    ResultSet consultarProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(idproveedor) as 'ID', concat(nombre_empresa) as 'Nombre empresa', concat(telefono) as 'Teléfono', " +
                "concat(ciudad) as 'Ciudad', concat(mercancia) as 'Mercancía', concat(codigo_postal) as 'Codigo postal'" +
                ", concat(fecha_pedido) as 'Fecha de pedido' , concat(precio) as 'Precio'" +
                " FROM proveedores";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Insertamos un animal
     * @param nombre
     * @param especie
     * @param identificador
     * @param edad
     * @param sexo
     * @param habitat
     */
    void insertarAnimal(String nombre, String especie, String identificador, int edad, String sexo, String habitat) {
        String sentenciaSql = "INSERT INTO animales (nombre, especie, identificador, edad, sexo, habitat) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, especie);
            sentencia.setString(3, identificador);
            sentencia.setInt(4, edad);
            sentencia.setString(5, sexo);
            sentencia.setString(6, habitat);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificamos un animal
     * @param nombre
     * @param especie
     * @param identificador
     * @param edad
     * @param sexo
     * @param habitat
     * @param idanimal
     */
    void modificarAnimal(String nombre, String especie, String identificador, int edad, String sexo, String habitat,int idanimal){

        String sentenciaSql = "UPDATE animales SET nombre = ?, especie = ?, identificador = ?, edad = ?, sexo = ?, habitat = ?" +
                "WHERE idanimal = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, especie);
            sentencia.setString(3, identificador);
            sentencia.setInt(4, edad);
            sentencia.setString(5, sexo);
            sentencia.setString(6, habitat);
            sentencia.setInt(7, idanimal);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Borramos un animal
     * @param idanimal
     */
    void borrarAnimal(int idanimal) {
        String sentenciaSql = "DELETE FROM animales WHERE idanimal = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idanimal);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Consultamos un animal
     * @return
     * @throws SQLException
     */
    ResultSet consultarAnimal() throws SQLException {
        String sentenciaSql = "SELECT concat(idanimal) as 'ID', concat(nombre) as 'Nombre animal', concat(especie) as 'Especie', " +
                "concat(identificador) as 'Identificador', concat(edad) as 'Edad', concat(sexo) as 'Sexo'" +
                ", concat(habitat) as 'Habitat' FROM animales";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Insertamos un registro
     * @param nombreRegistro
     * @param contrasenaRegistro
     * @param usuarioRegistro
     */
    void insertarRegistro(String nombreRegistro, String contrasenaRegistro, String usuarioRegistro ) {
        String sentenciaSql = "INSERT INTO login (nombre, contrasena, usuario) VALUES (?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombreRegistro);
            sentencia.setString(2, contrasenaRegistro);
            sentencia.setString(3, usuarioRegistro);

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Lee el archivo de propiedades
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Datos de nuestra base de datos
     * @param ip
     * @param user
     * @param pass
     * @param adminPass
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Comprobamos si existe el nombre del Proveedor en la BBDD
     * @param nombre_empresa
     * @return
     */
    public boolean proveedorYaExiste(String nombre_empresa) {
        String editorialNameConsult = "SELECT existeNombreProveedor(?)";
        PreparedStatement function;
        boolean existe = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre_empresa);
            ResultSet rs = function.executeQuery();
            rs.next();

            existe = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Comprobamos si exsite el nombre del empleado en la BBDD
     * @param nombre
     * @param apellidos
     * @return
     */
    public boolean empleadoNombreYaExiste(String nombre, String apellidos) {
        String completeName = apellidos + ", " + nombre;
        String authorNameConsult = "SELECT existeNombreEmpleado(?)";
        PreparedStatement function;
        boolean existe = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            existe = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Comprobamos si existe el identificador del animal
     * @param identificador
     * @return
     */
    public boolean animalIdentificadorYaExiste(String identificador) {
        String salesConsult = "SELECT existeIdentificadorAnimal(?)";
        PreparedStatement function;
        boolean identificadorExiste = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, identificador);
            ResultSet rs = function.executeQuery();
            rs.next();

            identificadorExiste = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return identificadorExiste;
    }

    /**
     * Comprobamos si existe el DNI del cliente
     * @param dni
     * @return
     */
    public boolean clienteDniYaExiste(String dni) {
        String salesConsult = "SELECT existeDniCliente(?)";
        PreparedStatement function;
        boolean dniExiste = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            dniExiste = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniExiste;
    }

    /**
     * Realizamos la busqueda por nombre del animal
     * @param nombre
     * @return
     * @throws SQLException
     */
    public ResultSet buscarAnimal(String nombre) throws SQLException {
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT idanimal, nombre, especie, identificador, edad, sexo, habitat FROM animales WHERE nombre = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(nombre));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Realizamos la busqueda por DNI del cliente
     * @param dni
     * @return
     * @throws SQLException
     */
    public ResultSet buscarDNI(String dni) throws SQLException {
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT idcliente, nombre, apellidos,dni,telefono, email, direccion, fecha_nacimiento FROM clientes WHERE dni = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(dni));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public void generarFactura() {
        try {

            // Carga del archivo .jasper
            JasperReport reporte = (JasperReport) JRLoader.loadObject(new File("jasper/Factura.jasper"));

            // crear conexion con la base de datos
            Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/tfgzoo", "root", "1234");

            // reporta los datos de la base de datos
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, conexion);

            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
            // Le indicamos donde se va a guardar y el nombre que va a recibir

            exporter.setParameter(JRExporterParameter.OUTPUT_FILE,new java.io.File("jasper/Pedido.pdf"));
            exporter.exportReport();

        } catch (JRException | SQLException jr) {
            jr.printStackTrace();
        }
    }
}
