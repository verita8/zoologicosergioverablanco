package gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Proveedor extends JFrame{
    private JTabbedPane tabbedPane1;
    private final static String TITULOFRAME = "Acceso Proveedores";
    private JPanel panelProveedores;
    public JTextField txtNombreProveedor;
    public JTextField txtTelefonoProveedor;
    public JTextField txtCiudad;
    public JTextField txtMercancia;
    public JTextField txtCodigoPostal;
    public JTextField txtPrecio;
    public JButton btnProveedoresAnadir;
    public JTable proveedoresProveedoresTabla;
    public DatePicker fechaProveedores;
    public JButton btnAtrasProveedor;

    public DefaultTableModel dtmProveedoresProveedores;

    public Proveedor() {
        super(TITULOFRAME);
        initFrame();
    }
    private void initFrame(){
        this.setContentPane(panelProveedores);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        //this.setIconImage(new ImageIcon(getClass().getResource("/logoZOO.PNG")).getImage());
        this.setLocationRelativeTo(null);
        this.setTableModels();
    }
    private void setTableModels() {
        this.dtmProveedoresProveedores = new DefaultTableModel();
        this.proveedoresProveedoresTabla.setModel(dtmProveedoresProveedores);
    }

}
