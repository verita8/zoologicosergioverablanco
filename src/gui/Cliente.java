package gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Cliente extends JFrame{
    private final static String TITULOFRAME = "Acceso clientes";
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    public JTable clientesBuscar;
    public JButton btnBuscarPorDni;
    public JTextField txtBuscarDni;
    public JButton btnAtrasCliente;
    public JButton btnRecargarDatos;
    public JTable clientesBuscarFiltrado;

    DefaultTableModel dtmClientes;
    DefaultTableModel dtmClientesFiltrado;

    public Cliente() {
        super(TITULOFRAME);
        initFrame();
    }
    private void initFrame(){
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        //this.setIconImage(new ImageIcon(getClass().getResource("/logoZOO.PNG")).getImage());
        this.setLocationRelativeTo(null);
        this.setTableModels();
    }
    private void setTableModels() {
        this.dtmClientes = new DefaultTableModel();
        this.clientesBuscar.setModel(dtmClientes);

        this.dtmClientesFiltrado = new DefaultTableModel();
        this.clientesBuscarFiltrado.setModel(dtmClientesFiltrado);
    }
}
