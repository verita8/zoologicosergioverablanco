package gui;

import baseEnums.EnumUsuarios;
import hash.Hash;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.File;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    private Login login;
    private Cliente cliente;
    private Proveedor proveedor;
    private Hash hash;
    boolean refrescar, iniciar;

    public Controlador(Modelo modelo, Vista vista, Login login, Cliente cliente, Proveedor proveedor) {
        this.modelo = modelo;
        this.vista = vista;
        this.login = login;
        this.proveedor = proveedor;
        this.cliente = cliente;
        iniciarTablas();
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    public void iniciarTablas() {
        iniciarTablaBuscarAnimal();
        iniciarTablaBuscarCliente();
        iniciar = true;
    }

    /**
     * Refresca los datos de la BBDD
     */
    private void refrescarTodo() {
        refrescarEmpleados();
        refrescarClientes();
        refrescarProveedor();
        refrescarAnimales();
        refrescarClienteDniBuscar();
        refrescarAnimalesBuscar();
        refrescarProveedorProveedor();
        refrescar = false;
    }

    /**
     * Le damos funcionalidad al clickar los botones
     *
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnProveedoresAnadir.addActionListener(listener);
        vista.btnProveedoresEliminar.addActionListener(listener);
        vista.btnProveedoresModificar.addActionListener(listener);
        vista.btnClientesAnadir.addActionListener(listener);
        vista.btnClientesEliminar.addActionListener(listener);
        vista.btnClientesModificar.addActionListener(listener);
        vista.btnEmpleadosAnadir.addActionListener(listener);
        vista.btnEmpleadosEliminar.addActionListener(listener);
        vista.btnEmpleadosModificar.addActionListener(listener);
        vista.btnAnimalesAnadir.addActionListener(listener);
        vista.btnAnimalesEliminar.addActionListener(listener);
        vista.btnAnimalesModificar.addActionListener(listener);
        vista.btnGenerarFactura.addActionListener(listener);
        vista.btnNavegarLogin.addActionListener(listener);

        //CLIENTE
        cliente.btnAtrasCliente.addActionListener(listener);
        cliente.btnBuscarPorDni.addActionListener(listener);

        //PROVEEDOR
        proveedor.btnAtrasProveedor.addActionListener(listener);
        proveedor.btnProveedoresAnadir.addActionListener(listener);

        //BUSCAR
        vista.btnBuscarNombreAnimal.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);

        //REGISTRO
        vista.btnRegistrarse.addActionListener(listener);

        //LOGIN
        login.btnAceptar.addActionListener(listener);


    }
/*
    public void elegirFila() {
        vista.empleadosTabla.addMouseListener(new

         MouseAdapter() {
         public void mouseClicked (MouseEvent e){
          int row = vista.empleadosTabla.getSelectedRow();
               vista.txtNombreEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 1)));
               vista.txtApellidosEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 2)));
               vista.fechaEmpleado.setDate((Date.valueOf(String.valueOf(vista.empleadosTabla.getValueAt(row, 3)))).toLocalDate());
               vista.txtTelefonoEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 4)));
               vista.txtDniEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 5)));
               vista.txtDireccionEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 6)));
               vista.txtSegudiradSocial.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 7)));
               vista.comboFuncion.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 8)));
               vista.txtEmailEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 9)));

 */



    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                int row = vista.empleadosTabla.getSelectedRow();
                vista.txtNombreEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 1)));
                vista.txtApellidosEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 2)));
                vista.fechaEmpleado.setDate((Date.valueOf(String.valueOf(vista.empleadosTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtTelefonoEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 4)));
                vista.txtDniEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 5)));
                vista.txtDireccionEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 6)));
                vista.txtSegudiradSocial.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 7)));
                vista.comboFuncion.setSelectedItem(String.valueOf(vista.empleadosTabla.getValueAt(row, 8)));
                vista.txtEmailEmpleado.setText(String.valueOf(vista.empleadosTabla.getValueAt(row, 9)));

            } else if (e.getSource().equals(vista.clientesTabla.getSelectionModel())) {
                int row = vista.clientesTabla.getSelectedRow();
                vista.txtNombreCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 1)));
                vista.txtApellidosCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 2)));
                vista.txtDniCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 3)));
                vista.txtTelefonoCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 4)));
                vista.txtEmailCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 5)));
                vista.txtDireccionCliente.setText(String.valueOf(vista.clientesTabla.getValueAt(row, 6)));
                vista.fechaCliente.setDate((Date.valueOf(String.valueOf(vista.clientesTabla.getValueAt(row, 7)))).toLocalDate());
            } else if (e.getSource().equals(vista.proveedoresTabla.getSelectionModel())) {
                int row = vista.proveedoresTabla.getSelectedRow();
                vista.txtNombreProveedor.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 1)));
                vista.txtTelefonoProveedor.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 2)));
                vista.txtCiudad.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 3)));
                vista.txtMercancia.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 4)));
                vista.txtCodigoPostal.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 5)));
                vista.txtPrecio.setText(String.valueOf(vista.proveedoresTabla.getValueAt(row, 6)));
                vista.fechaProveedores.setDate((Date.valueOf(String.valueOf(vista.proveedoresTabla.getValueAt(row, 7)))).toLocalDate());
            } else if (e.getSource().equals(proveedor.proveedoresProveedoresTabla.getSelectionModel())) {
                int row = proveedor.proveedoresProveedoresTabla.getSelectedRow();
                proveedor.txtNombreProveedor.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 1)));
                proveedor.txtTelefonoProveedor.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 2)));
                proveedor.txtCiudad.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 3)));
                proveedor.txtMercancia.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 4)));
                proveedor.txtCodigoPostal.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 5)));
                proveedor.txtPrecio.setText(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 6)));
                proveedor.fechaProveedores.setDate((Date.valueOf(String.valueOf(proveedor.proveedoresProveedoresTabla.getValueAt(row, 7)))).toLocalDate());
            } else if (e.getSource().equals(vista.animalesTabla.getSelectionModel())) {
                int row = vista.animalesTabla.getSelectedRow();
                vista.txtNombreAnimal.setText(String.valueOf(vista.animalesTabla.getValueAt(row, 1)));
                vista.txtEspecie.setText(String.valueOf(vista.animalesTabla.getValueAt(row, 2)));
                vista.txtIdentificador.setText(String.valueOf(vista.animalesTabla.getValueAt(row, 3)));
                vista.txtEdad.setText(String.valueOf(vista.animalesTabla.getValueAt(row, 4)));
                vista.txtSexo.setText(String.valueOf(vista.animalesTabla.getValueAt(row, 5)));
                vista.comboHabitat.setSelectedItem(String.valueOf(vista.animalesTabla.getValueAt(row, 6)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.empleadosTabla.getSelectionModel())) {
                    borrarCamposEmpleados();
                } else if (e.getSource().equals(vista.clientesTabla.getSelectionModel())) {
                    borrarCamposClientes();
                } else if (e.getSource().equals(vista.proveedoresTabla.getSelectionModel())) {
                    borrarCamposProveedores();
                } else if (e.getSource().equals(proveedor.proveedoresProveedoresTabla.getSelectionModel())) {
                    borrarCamposProveedores();
                } else if (e.getSource().equals(vista.animalesTabla.getSelectionModel())) {
                    borrarCamposAnimales();
                }
            }
        }
    }

    /**
     * Enlazamos los botones con la función que queremos que realice
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista(), new Login(), new Cliente(), new Proveedor());
                break;
            case "buscarDniCliente":
                String buscar=cliente.txtBuscarDni.getText();
                try {
                    ResultSet rs =modelo.buscarDNI(buscar);
                    refrescarClienteDniBuscar();
                    cargarClientes(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "buscarNombreAnimal":
                String buscarAnimal=vista.txtBuscar.getText();
                try {
                    ResultSet rs =modelo.buscarAnimal(buscarAnimal);
                    refrescarAnimalesBuscar();
                    cargarAnimales(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "iniciarSesion":
                vista.setVisible(false);
                login.setVisible(true);
                break;
            case "atrasCliente":
                cliente.setVisible(false);
                login.setVisible(true);
                break;
            case "atrasProveedor":
                proveedor.setVisible(false);
                login.setVisible(true);
                break;

            case "anadirEmpleado": {
                try {
                    if (comprobarEmpleadoVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else if (modelo.empleadoNombreYaExiste(vista.txtNombreEmpleado.getText(), vista.txtApellidosEmpleado.getText())) {
                        Util.showErrorAlert("Este nombre ya ha sido registrado.\nIntroduce un nombre diferente");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.insertarEmpleado(
                                vista.txtNombreEmpleado.getText(),
                                vista.txtApellidosEmpleado.getText(),
                                vista.fechaEmpleado.getDate(),
                                Integer.parseInt(vista.txtTelefonoEmpleado.getText()),
                                vista.txtDniEmpleado.getText(),
                                vista.txtDireccionEmpleado.getText(),
                                Integer.parseInt(vista.txtSegudiradSocial.getText()),
                                String.valueOf(vista.comboFuncion.getSelectedItem()),
                                vista.txtEmailEmpleado.getText());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleados();
                refrescarEmpleados();
            }
            break;
            case "modificarEmpleado": {
                try {
                    if (comprobarEmpleadoVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.empleadosTabla.clearSelection();
                    } else {
                        modelo.modificarEmpleado(
                                vista.txtNombreEmpleado.getText(),
                                vista.txtApellidosEmpleado.getText(),
                                vista.fechaEmpleado.getDate(),
                                Integer.parseInt(vista.txtTelefonoEmpleado.getText()),
                                vista.txtDniEmpleado.getText(),
                                vista.txtDireccionEmpleado.getText(),
                                Integer.parseInt(vista.txtSegudiradSocial.getText()),
                                String.valueOf(vista.comboFuncion.getSelectedItem()),
                                vista.txtEmailEmpleado.getText(),
                                Integer.parseInt((String)vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.empleadosTabla.clearSelection();
                }
                borrarCamposEmpleados();
                refrescarEmpleados();
            }
            break;
            case "eliminarEmpleado":
                modelo.borrarEmpleado(Integer.parseInt((String)vista.empleadosTabla.getValueAt(vista.empleadosTabla.getSelectedRow(), 0)));
                borrarCamposEmpleados();
                refrescarEmpleados();
                break;
            case "anadirCliente": {
                try {
                    if (comprobarClienteVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else if (modelo.clienteDniYaExiste(vista.txtDniCliente.getText())) {
                        Util.showErrorAlert("El DNI del cliente ya existe.\nIntroduce un DNI diferente");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.insertarCliente(
                                vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.fechaCliente.getDate(),
                                Integer.parseInt(vista.txtTelefonoCliente.getText()),
                                vista.txtDniCliente.getText(),
                                vista.txtDireccionCliente.getText(),
                                vista.txtEmailCliente.getText());
                        refrescarClientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "modificarCliente": {
                try {
                    if (comprobarClienteVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clientesTabla.clearSelection();
                    } else {
                        modelo.modificarCliente(
                                vista.txtNombreCliente.getText(),
                                vista.txtApellidosCliente.getText(),
                                vista.fechaCliente.getDate(),
                                Integer.parseInt(vista.txtTelefonoCliente.getText()),
                                vista.txtDniCliente.getText(),
                                vista.txtDireccionCliente.getText(),
                                vista.txtEmailCliente.getText(),
                                Integer.parseInt((String)vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                        refrescarClientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.clientesTabla.clearSelection();
                }
                borrarCamposClientes();
            }
            break;
            case "eliminarCliente":
                modelo.borrarCliente(Integer.parseInt((String)vista.clientesTabla.getValueAt(vista.clientesTabla.getSelectedRow(), 0)));
                borrarCamposClientes();
                refrescarClientes();
                break;

            case "anadirProveedorAdmin": {
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.proveedoresTabla.clearSelection();
                    } else if (modelo.proveedorYaExiste(vista.txtNombreProveedor.getText())) {
                        Util.showErrorAlert("El nombre de empresa del Proveedor ya existe.\nIntroduce un nombre diferente");
                        vista.proveedoresTabla.clearSelection();
                    } else {
                        modelo.insertarProveedor(
                                vista.txtNombreProveedor.getText(),
                                Integer.parseInt(vista.txtTelefonoProveedor.getText()),
                                vista.txtCiudad.getText(),
                                vista.txtMercancia.getText(),
                                vista.fechaProveedores.getDate(),
                                vista.txtCodigoPostal.getText(),
                                Float.parseFloat(vista.txtPrecio.getText()));
                        refrescarProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.proveedoresTabla.clearSelection();
                }
                borrarCamposProveedores();
            }
            break;
            case "modificarProveedorAdmin": {
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.proveedoresTabla.clearSelection();
                    } else {
                        modelo.modificarProveedor(
                                vista.txtNombreProveedor.getText(),
                                Integer.parseInt(vista.txtTelefonoProveedor.getText()),
                                vista.txtCiudad.getText(),
                                vista.txtMercancia.getText(),
                                vista.fechaProveedores.getDate(),
                                vista.txtCodigoPostal.getText(),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                Integer.parseInt((String)vista.proveedoresTabla.getValueAt(vista.proveedoresTabla.getSelectedRow(), 0)));
                        refrescarProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.proveedoresTabla.clearSelection();
                }
                borrarCamposProveedores();
            }
            break;
            case "eliminarProveedorAdmin":
                modelo.borrarProveedor(Integer.parseInt((String)vista.proveedoresTabla.getValueAt(vista.proveedoresTabla.getSelectedRow(), 0)));
                borrarCamposProveedores();
                refrescarProveedor();
                break;
            case "generarFactura":
                modelo.generarFactura();
                break;

            case "anadirProveedor": {
                try {
                    if (comprobarProveedorProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        proveedor.proveedoresProveedoresTabla.clearSelection();
                    } else if (modelo.proveedorYaExiste(proveedor.txtNombreProveedor.getText())) {
                        Util.showErrorAlert("El nombre de empresa del Proveedor ya existe.\nIntroduce un nombre diferente");
                        proveedor.proveedoresProveedoresTabla.clearSelection();
                    } else {
                        modelo.insertarProveedor(
                                proveedor.txtNombreProveedor.getText(),
                                Integer.parseInt(proveedor.txtTelefonoProveedor.getText()),
                                proveedor.txtCiudad.getText(),
                                proveedor.txtMercancia.getText(),
                                proveedor.fechaProveedores.getDate(),
                                proveedor.txtCodigoPostal.getText(),
                                Float.parseFloat(proveedor.txtPrecio.getText()));
                        refrescarProveedorProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    proveedor.proveedoresProveedoresTabla.clearSelection();
                }
                borrarCamposProveedoresProveedores();
            }
            break;

            case "anadirAnimal": {
                try {
                    if (comprobarAnimalVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.animalesTabla.clearSelection();
                    } else if (modelo.animalIdentificadorYaExiste(vista.txtIdentificador.getText())) {
                        Util.showErrorAlert("El Identificador del animal ya existe.\nIntroduce un identificador diferente");
                        vista.animalesTabla.clearSelection();
                    } else {
                        modelo.insertarAnimal(
                                vista.txtNombreAnimal.getText(),
                                vista.txtEspecie.getText(),
                                vista.txtIdentificador.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.txtSexo.getText(),
                                String.valueOf(vista.comboHabitat.getSelectedItem()));
                        refrescarAnimales();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.animalesTabla.clearSelection();
                }
                borrarCamposAnimales();
            }
            break;
            case "modificarAnimal": {
                try {
                    if (comprobarAnimalVacio(vista)) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.animalesTabla.clearSelection();
                    } else {
                        modelo.modificarAnimal(
                                vista.txtNombreAnimal.getText(),
                                vista.txtEspecie.getText(),
                                vista.txtIdentificador.getText(),
                                Integer.parseInt(vista.txtEdad.getText()),
                                vista.txtSexo.getText(),
                                String.valueOf(vista.comboHabitat.getSelectedItem()),
                                Integer.parseInt((String)vista.animalesTabla.getValueAt(vista.animalesTabla.getSelectedRow(), 0)));
                        refrescarAnimales();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                    vista.animalesTabla.clearSelection();
                }
                borrarCamposAnimales();
            }
            break;
            case "eliminarAnimal":
                modelo.borrarAnimal(Integer.parseInt((String)vista.animalesTabla.getValueAt(vista.animalesTabla.getSelectedRow(), 0)));
                borrarCamposAnimales();
                refrescarAnimales();
                break;

            case "aceptarUsuario":
                //String contrasena = new String(login.txtContrasena.getText());

                if(comprobarLoginVacio()){
                    Util.showErrorAlert("Error, debes rellenar todos los campos.");
                } else {

                    String contrasenaHash = Hash.sha1(login.txtContrasena.getText());
                    boolean comprobarLogin = modelo.comprobarDatos(login.txtUsuario.getText(), contrasenaHash, String.valueOf(login.comboUsuarios.getSelectedItem()));

                    System.out.println(comprobarLogin);
                    if (comprobarLogin) {
                        System.out.println(login.comboUsuarios.getSelectedItem() + " " + EnumUsuarios.ADMINISTRADOR);
                        if (login.comboUsuarios.getSelectedItem().equals(EnumUsuarios.ADMINISTRADOR.getValor())) {
                            vista.dispose();
                            vista.setVisible(true);
                            login.setVisible(false);
                            //login.dispose();
                        } else if (login.comboUsuarios.getSelectedItem().equals(EnumUsuarios.CLIENTE.getValor())) {
                            cliente.setVisible(true);
                            login.setVisible(false);
                            //login.dispose();
                        } else if (login.comboUsuarios.getSelectedItem().equals(EnumUsuarios.PROVEEDOR.getValor())) {
                            proveedor.setVisible(true);
                            login.setVisible(false);
                            //login.dispose();
                        }else{
                            Util.showErrorAlert("ERROR.");
                        }
                    }else{
                        Util.showErrorAlert("ERROR. Datos de inicio de sesión incorrectos.");
                    }
                }
                break;

            case "anadirRegistro": {
                try {
                    if (comprobarRegistroVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        String contrasena = new String(vista.txtContrasenaRegistro.getText());
                        String contrasenaHash = Hash.sha1(contrasena);
                        System.out.println(contrasenaHash);
                        modelo.insertarRegistro(
                                vista.txtUsuarioRegistro.getText(),
                                contrasenaHash,
                                String.valueOf(vista.comboUsuarioRegistro.getSelectedItem()));


                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce información en los campos que lo requieren");
                }
                borrarCamposRegistro();
            }
            break;
        }


    }

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[5];
        vista.dtmBuscarAnimales.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);

            vista.dtmBuscarAnimales.addRow(fila);
        }
    }

    /**
     * Refresca el DTM de empleados
     */
    private void refrescarEmpleados() {
        try {
            vista.empleadosTabla.setModel(construirTableModelEmpleados(modelo.consultarEmpleado()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM de empleados
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelEmpleados(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmEmpleados.setDataVector(data, columnNames);

        return vista.dtmEmpleados;
    }

    /**
     * Refresca el DTM de clientes
     */
    private void refrescarClientes() {
        try {
            vista.clientesTabla.setModel(construirTableModelClientes(modelo.consultarCliente()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM de clientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelClientes(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmClientes.setDataVector(data, columnNames);

        return vista.dtmClientes;
    }

    /**
     * Refresca el DTM de proveedores
     */
    private void refrescarProveedor() {
        try {
            vista.proveedoresTabla.setModel(construirTableModelProveedores(modelo.consultarProveedor()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM de proveedores
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProveedores(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmProveedores.setDataVector(data, columnNames);

        return vista.dtmProveedores;
    }

    /**
     * Refresca el DTM del usuario de proveedores
     */
    private void refrescarProveedorProveedor() {
        try {
            proveedor.proveedoresProveedoresTabla.setModel(construirTableModelProveedoresProveedores(modelo.consultarProveedor()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM del usuario de proveedores
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProveedoresProveedores(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        proveedor.dtmProveedoresProveedores.setDataVector(data, columnNames);

        return proveedor.dtmProveedoresProveedores;
    }

    /**
     * Refresca el DTM de animales
     */
    private void refrescarAnimales() {
        try {
            vista.animalesTabla.setModel(construirTableModelAnimales(modelo.consultarAnimal()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM de animales
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelAnimales(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmAnimales.setDataVector(data, columnNames);

        return vista.dtmAnimales;
    }

    /**
     * Refresca el DTM de la busqueda de animales
     */
    private void refrescarAnimalesBuscar() {
        try {
            vista.animalesBuscar.setModel(construirTableModelAnimalesBuscar(modelo.consultarAnimal()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye el DTM de la busqueda de animales
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelAnimalesBuscar(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmBuscarAnimales.setDataVector(data, columnNames);

        return vista.dtmBuscarAnimales;
    }

    /**
     * Refresca el DTM de la busqueda de clientes
     */
    private void refrescarClienteDniBuscar() {
        try {
            cliente.clientesBuscar.setModel(construirTableModelClientesBuscar(modelo.consultarCliente()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Construye el DTM de busqueda de clientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelClientesBuscar(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmClientesFiltrado.setDataVector(data, columnNames);

        return cliente.dtmClientesFiltrado;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Borra los campos de empleados
     */
    private void borrarCamposEmpleados() {
        vista.comboFuncion.setSelectedIndex(-1);
        vista.txtNombreEmpleado.setText("");
        vista.txtApellidosEmpleado.setText("");
        vista.txtTelefonoEmpleado.setText("");
        vista.txtDniEmpleado.setText("");
        vista.txtDireccionEmpleado.setText("");
        vista.txtSegudiradSocial.setText("");
        vista.txtEmailEmpleado.setText("");
        vista.fechaEmpleado.setText("");
    }

    /**
     * Borra los campos de clientes
     */
    private void borrarCamposClientes() {
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtTelefonoCliente.setText("");
        vista.txtDniCliente.setText("");
        vista.txtDireccionCliente.setText("");
        vista.txtEmailCliente.setText("");
        vista.fechaCliente.setText("");
    }

    /**
     * Borra los camos de proveedores
     */
    private void borrarCamposProveedores() {
        vista.txtNombreProveedor.setText("");
        vista.txtTelefonoProveedor.setText("");
        vista.txtCiudad.setText("");
        vista.txtMercancia.setText("");
        vista.txtCodigoPostal.setText("");
        vista.txtPrecio.setText("");
        vista.fechaProveedores.setText("");
    }

    /**
     * Borra los campos del usuario de proveedores
     */
    private void borrarCamposProveedoresProveedores() {
        proveedor.txtNombreProveedor.setText("");
        proveedor.txtTelefonoProveedor.setText("");
        proveedor.txtCiudad.setText("");
        proveedor.txtMercancia.setText("");
        proveedor.txtCodigoPostal.setText("");
        proveedor.txtPrecio.setText("");
        proveedor.fechaProveedores.setText("");
    }

    /**
     * Borra los camos de animales
     */
    private void borrarCamposAnimales() {
        vista.comboHabitat.setSelectedIndex(-1);
        vista.txtNombreAnimal.setText("");
        vista.txtEspecie.setText("");
        vista.txtIdentificador.setText("");
        vista.txtEdad.setText("");
        vista.txtSexo.setText("");
    }

    /**
     * Borra los campos del registro de usuarios
     */
    private void borrarCamposRegistro() {
        vista.comboUsuarioRegistro.setSelectedIndex(-1);
        vista.txtUsuarioRegistro.setText("");
        vista.txtContrasenaRegistro.setText("");
    }

    /**
     * Comprueba los campos vacios de empleados
     * @return
     */
    public boolean comprobarEmpleadoVacio(Vista vista) {
        return vista.txtNombreEmpleado.getText().isEmpty() ||
                vista.txtApellidosEmpleado.getText().isEmpty() ||
                vista.fechaEmpleado.getText().isEmpty() ||
                vista.txtTelefonoEmpleado.getText().isEmpty() ||
                vista.txtDniEmpleado.getText().isEmpty() ||
                vista.txtDireccionEmpleado.getText().isEmpty() ||
                vista.txtSegudiradSocial.getText().isEmpty() ||
                vista.comboFuncion.getSelectedIndex() == -1 ||
                vista.txtEmailEmpleado.getText().isEmpty();

    }
    /**
     * Comprueba los campos vacios de clientes
     * @return
     */
    public boolean comprobarClienteVacio(Vista vista) {
        return vista.txtNombreCliente.getText().isEmpty() ||
                vista.txtApellidosCliente.getText().isEmpty() ||
                vista.txtTelefonoCliente.getText().isEmpty() ||
                vista.txtDniCliente.getText().isEmpty() ||
                vista.txtDireccionCliente.getText().isEmpty() ||
                vista.txtEmailCliente.getText().isEmpty() ||
                vista.fechaCliente.getText().isEmpty();
    }
    /**
     * Comprueba los campos vacios de proveedores
     * @return
     */
    private boolean comprobarProveedorVacio() {
        return vista.txtNombreProveedor.getText().isEmpty() ||
                vista.txtTelefonoProveedor.getText().isEmpty() ||
                vista.txtCiudad.getText().isEmpty() ||
                vista.txtMercancia.getText().isEmpty() ||
                vista.txtCodigoPostal.getText().isEmpty() ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.fechaProveedores.getText().isEmpty();
    }

    /**
     * Comprueba los campos vacios del usuario de proveedores
     * @return
     */
    private boolean comprobarProveedorProveedorVacio() {
        return proveedor.txtNombreProveedor.getText().isEmpty() ||
                proveedor.txtTelefonoProveedor.getText().isEmpty() ||
                proveedor.txtCiudad.getText().isEmpty() ||
                proveedor.txtMercancia.getText().isEmpty() ||
                proveedor.txtCodigoPostal.getText().isEmpty() ||
                proveedor.txtPrecio.getText().isEmpty() ||
                proveedor.fechaProveedores.getText().isEmpty();
    }
    /**
     * Comprueba los campos vacios de animales
     * @return
     */
    public boolean comprobarAnimalVacio(Vista vista) {
        return
                vista.txtNombreAnimal.getText().isEmpty() ||
                vista.txtEspecie.getText().isEmpty() ||
                vista.txtIdentificador.getText().isEmpty() ||
                vista.txtEdad.getText().isEmpty() ||
                vista.txtSexo.getText().isEmpty() ||
                vista.comboHabitat.getSelectedIndex() == -1;
    }
    /**
     * Comprueba los campos vacios del registro de usuarios
     * @return
     */
    private boolean comprobarRegistroVacio() {
        return
                vista.txtUsuarioRegistro.getText().isEmpty() ||
                        vista.txtContrasenaRegistro.getText().isEmpty() ||
                        vista.comboUsuarioRegistro.getSelectedIndex() == -1;
    }

    /**
     * Comprueba los campos vacios del login
     * @return
     */
    private boolean comprobarLoginVacio() {
        return login.comboUsuarios.getSelectedIndex() == -1 ||
                login.txtUsuario.getText().isEmpty() ||
                login.txtContrasena.getText().isEmpty();
    }

    /**
     * Inicia la tabla de busqueda de animales
     */
    private void iniciarTablaBuscarAnimal() {
        String[] headers = { "Nombre", "Especie","Identificador", "Edad", "Sexo"};
        vista.dtmBuscarAnimales.setColumnIdentifiers(headers);
    }
    /**
     * Carga los datos de la tabla de animales
     * @param resultSet
     * @throws SQLException
     */
    private void cargarAnimales(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[6];
        vista.dtmBuscarAnimales.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);

            vista.dtmBuscarAnimales.addRow(fila);
        }
    }

    /**
     * Inicia la tabla de busqueda de clientes
     */
    private void iniciarTablaBuscarCliente() {
        String[] headers = { "Nombre", "Apellidos","DNI", "Teléfono", "Email","Direccion", "Fecha de nacimiento"};
        cliente.dtmClientesFiltrado.setColumnIdentifiers(headers);
    }

    /**
     * Carga los datos de la tabla de clientes
     * @param resultSet
     * @throws SQLException
     */

    private void cargarClientes(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtmClientes.setRowCount(0);
        cliente.dtmClientesFiltrado.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);

            cliente.dtmClientesFiltrado.addRow(fila);

        }
    }

    private void addWindowListeners(Controlador controlador) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
    private void addItemListeners(Controlador controlador) {
    }
    /**
     * Realiza un autoguardado cuando la ventana se cierra
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
    @Override
    public void windowOpened(WindowEvent e) {

    }
}
