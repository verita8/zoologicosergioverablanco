package pruebas;

import gui.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ControladorTest {

    @BeforeEach
    void setUp() {

    }

    Vista vista = new Vista();
    Login login = new Login();
    Cliente cliente = new Cliente();
    Proveedor proveedor = new Proveedor();
    Modelo modelo = new Modelo();

    @Test
    void comprobarEmpleadoNoVacio() {
        vista.txtNombreEmpleado.setText("Angel");
        vista.txtApellidosEmpleado.setText("lalallalala");
        vista.fechaEmpleado.setText("2021/11/11");
        vista.txtTelefonoEmpleado.setText("654654654");
        vista.txtDniEmpleado.setText("11111111H");
        vista.txtDireccionEmpleado.setText("calle");
        vista.txtSegudiradSocial.setText("95856586");
        vista.comboFuncion.setSelectedIndex(1);
        vista.txtEmailEmpleado.setText("aaa@aaa.es");
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertFalse(testControlador.comprobarEmpleadoVacio(vista));
    }

    @Test
    void comprobarEmpleadoConCamposVacios() {
        vista.txtNombreEmpleado.setText("Sergio");
        vista.txtApellidosEmpleado.setText("lalallalala");
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertTrue(testControlador.comprobarEmpleadoVacio(vista));
    }

    @Test
    void comprobarAnimalNoVacio() {
        vista.txtNombreAnimal.setText("Simba");
        vista.txtEspecie.setText("Leon");
        vista.txtIdentificador.setText("QWFE532");
        vista.txtEdad.setText("3");
        vista.txtSexo.setText("M");
        vista.comboHabitat.setSelectedItem(1);
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertTrue(testControlador.comprobarAnimalVacio(vista));
    }

    @Test
    void comprobarAnimalConCamposVacios() {
        vista.txtNombreAnimal.setText("Bellota");
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertTrue(testControlador.comprobarAnimalVacio(vista));
    }
    @Test
    void comprobarClienteLleno() {
        vista.txtNombreCliente.getText().isEmpty();
        vista.txtApellidosCliente.setText("Victor");
        vista.txtTelefonoCliente.setText("Vera");
        vista.txtDniCliente.setText("23556447G");
        vista.txtDireccionCliente.setText("C/Sagrado");
        vista.txtEmailCliente.setText("victor@gmail.com");
        vista.fechaCliente.setText("2021/11/11");
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertTrue(testControlador.comprobarClienteVacio(vista));
    }

    @Test
    void comprobarClienteVacio() {
        vista.txtNombreCliente.getText().isEmpty();
        vista.txtApellidosCliente.setText("Victor");
        Controlador testControlador = new Controlador(modelo, vista, login , cliente, proveedor);
        assertTrue(testControlador.comprobarClienteVacio(vista));
    }
}