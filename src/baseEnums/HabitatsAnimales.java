package baseEnums;

public enum HabitatsAnimales {
    PRADERA("Pradera"),
    BOSQUE("Bosque"),
    DESIERTO("Desierto"),
    MONTAÑA("Montaña"),
    MARISMA("Marisma"),
    SABANA("Sabana"),
    POLAR("Región polar"),
    QUEBRADA("Quebrada"),
    ARRECIFE("Arrecife de coral");

    public String valor;

    HabitatsAnimales(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
