package baseEnums;

public enum EnumUsuarios {
    ADMINISTRADOR("Administrador"),
    CLIENTE("Cliente"),
    PROVEEDOR("Proveedor");

    private String valor;

    EnumUsuarios(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
