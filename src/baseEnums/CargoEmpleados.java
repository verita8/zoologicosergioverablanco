package baseEnums;

public enum CargoEmpleados {
    CEO("Ceo"),
    GERENTE("Gerente"),
    JEFE("Jefe"),
    VETERINARIO("Equipo veterinario"),
    COMUNICACIONES("Equipo de comunicaciones"),
    SERVICIO("Equipo de servicio al cliente"),
    ADMINISTRATIVO("Equipo administrativo y financiero"),
    BIOLOGIA("Equipo de biología");

    private String valor;

    CargoEmpleados(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
