CREATE DATABASE IF NOT EXISTS tfgzoo;
USE tfgzoo;

CREATE TABLE IF NOT EXISTS empleados
(
	
    idempleado int auto_increment primary key,
	nombre VARCHAR(20) NOT NULL,
	apellidos VARCHAR(30) NOT NULL,
	fecha_nacimiento date,
	telefono integer,
	dni VARCHAR(10) NOT NULL UNIQUE,
	direccion VARCHAR(50),
	seguridad_social integer,
	funcion VARCHAR(50),
	email VARCHAR(25)
);
CREATE TABLE IF NOT EXISTS clientes 
(
	idcliente int auto_increment primary key,
   	nombre VARCHAR(30),
	apellidos VARCHAR(40),
	dni VARCHAR(10) NOT NULL UNIQUE,
   	telefono integer,
	email VARCHAR(50),
	direccion VARCHAR(50),
	fecha_nacimiento date
);
CREATE TABLE IF NOT EXISTS proveedores
(
	idproveedor int auto_increment primary key,
	nombre_empresa VARCHAR(50) NOT NULL,
	telefono integer,
	ciudad VARCHAR(20),
	mercancia VARCHAR(20),
	codigo_postal VARCHAR(5) UNIQUE NOT NULL,
	fecha_pedido date,
	precio FLOAT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS animales
(
	idanimal int auto_increment primary key,
	nombre VARCHAR(20) NOT NULL,
	especie VARCHAR(50) NOT NULL,
	identificador VARCHAR(10) NOT NULL,
	edad integer,
	sexo VARCHAR(10),
	habitat VARCHAR(20)
);

create table if not exists login(
nombre varchar(30) NOT NULL,
contrasena varchar(30) not null,
usuario varchar(30) NOT NULL);
--
INSERT INTO login (nombre,contrasena,usuario) VALUES
('sergio', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'ADMINISTRADOR'),
('cliente', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'CLIENTE'),
('proveedor', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'PROVEEDOR');

delimiter ||
create function existeNombreProveedor(f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idproveedor) from proveedores)) do
    if  ((select nombre_empresa from proveedores where idproveedor = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreEmpleado(f_name varchar(100))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idempleado) from empleados)) do
    if  ((select concat(apellidos, ', ', nombre) from empleados where idempleado = (i + 1)) like f_name) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeIdentificadorAnimal(f_identificador varchar(20))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idanimal) from animales)) do
    if  ((select identificador from animales where idanimal = (i + 1)) like f_identificador) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;

delimiter ||
create function existeDniCliente(f_dni varchar(20))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idcliente) from clientes)) do
    if  ((select dni from clientes where idcliente = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;